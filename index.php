<p><b><a href="../">HOME</a> | <a href="../upload">UPLOAD FILE STRUCTURE</a></b></p><hr/>
<h1>NetPay Test</h1><br/>

 <form action="" method="POST">
  Search:<br>
  <input type="text" name="searchterm" placeholder="Search extension..." required>
  <input type="submit" value="Submit">
  <input type="reset">
</form> 

<?php
include "db.php";
if(isset($_POST["searchterm"])) {
    $searchterm = $_POST["searchterm"];
    unset($_POST["searchterm"]);
    $_POST["searchterm"] = NULL;
    $sql = "SELECT file_extension FROM file_structure WHERE `file_extension` LIKE '%{$searchterm}%'";
    if($result = $conn->query($sql)) {
        echo "<p><b>Search Results:</b></p>";
        while ($row = $result->fetch_assoc()) {
            echo $row["file_extension"]."<br/>";
        }
        if(mysqli_num_rows($result) == 0){ echo "<p style='color:red;'>No Results.</p>"; }
    }
    $result->close();
}
$conn->close();
?>